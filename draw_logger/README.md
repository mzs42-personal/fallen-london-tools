
Fallen London Draw Logger
=========================

Installation
------------

Don't. This has permissions that could seriously mess with your FL account and possibly break the TOS. Even if you trust me not to abuse these permissions, this is the first extension I've ever made and I can't guarantee I haven't messed something up.

If you can read the code to verify it for yourself or if you want to ignore the above warnings, installation is fairly simple:

1. Download the `src` directory
2. In Chrome, navigate to `chrome://extensions`
3. Make sure the `Developer mode` toggle is ON
4. Click `Load unpacked`
5. Navigate to the downloaded `src` directory and click `Select Folder`

That's it! If you already have Fallen London open, you'll need to refresh it for the logger to start working.

Use
---

The extension automatically logs all cards drawn and discarded. To view the logs, first make sure the active tab is open to Fallen London, click the hat icon, then click `View Log`. By default it shows a formatted, human-readable view of the collected data, but the page has a button to toggle to a raw JSON view. It also provides a button to clear the collected log.

Troubleshooting
---------------

If you have any problems with the Fallen London site while using this extension--*especially* problems related to API calls--please turn off this extension and confirm the problem before bothering the poor folks at Failbetter support. While I've attempted to minimise interference, the nature of this extension means that if it breaks, it can look exactly like the site itself breaking.


