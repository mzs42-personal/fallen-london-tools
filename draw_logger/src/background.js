chrome.webNavigation.onCommitted.addListener((details) => {
    const src_url = chrome.runtime.getURL('scripts/xhr.js');

    const script_code = `
      let myself_el = document.createElement('script');
      myself_el.id = 'opportunity-log';
      myself_el.type = 'application/json';
      myself_el.innerText = '[]';
      document.head.appendChild(myself_el);
      let s = document.createElement('script');
      s.src = "${src_url}";
      s.onload = function() {
          this.remove();
      };
      document.head.appendChild(s);
    `;

    chrome.tabs.executeScript(details.tabId, {code: script_code, runAt: 'document_start'});
}, {url: [{hostEquals: 'www.fallenlondon.com'}]});

chrome.runtime.onInstalled.addListener(function() {
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
    chrome.declarativeContent.onPageChanged.addRules([{
      conditions: [new chrome.declarativeContent.PageStateMatcher({
        pageUrl: {hostEquals: 'www.fallenlondon.com'},
      })
      ],
        actions: [new chrome.declarativeContent.ShowPageAction()]
    }]);
  });
});