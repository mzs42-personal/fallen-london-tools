const flDrawDataOrigXHR = window.XMLHttpRequest;

window.XMLHttpRequest = class extends flDrawDataOrigXHR {
    open(method, url, opt_async, opt_user, opt_password) {
        this.is_discard = false;
        if (url === 'https://api.fallenlondon.com/api/opportunity/draw') {
            this.addEventListener('load', () => {
                try {
                    const log_el = document.getElementById('opportunity-log');
                    const opportunity_log = JSON.parse(log_el.innerText);
                    const resp = JSON.parse(this.response);
                    const new_card = resp.displayCards[resp.displayCards.length - 1];
                    opportunity_log.push({timestamp: Date.now(), card_id: new_card.eventId, card_name: new_card.name, operation: 'draw'});
                    log_el.innerText = JSON.stringify(opportunity_log);
                    log_el.dispatchEvent(new Event('updatejson'));
                } catch (error) {
                    console.error(error);
                }
            });
        } else if (url === 'https://api.fallenlondon.com/api/opportunity/discard') {
            this.is_discard = true;
            this.addEventListener('load', () => {
                try {
                    if (this.card_data) {
                        const log_el = document.getElementById('opportunity-log');
                        const opportunity_log = JSON.parse(log_el.innerText);
                        this.card_data.timestamp = Date.now();
                        opportunity_log.push(this.card_data);
                        this.card_data = undefined;
                        log_el.innerText = JSON.stringify(opportunity_log);
                        log_el.dispatchEvent(new Event('updatejson'));
                    }
                } catch (error) {
                    console.error(error);
                }
            });
        }
        super.open(method, url, opt_async, opt_user, opt_password);
    }
    
    send(opt_body) {
        try {
            if (this.is_discard && opt_body && JSON.parse(opt_body).eventId != null) {
                const eventId = JSON.parse(opt_body).eventId;
                this.card_data = {
                    card_id: eventId,
                    card_name: document.querySelector(`.hand__card-container[data-event-id="${eventId}"] img`).ariaLabel,
                    operation: 'discard',
                };
            }
        } catch (error) {
            console.error(error);
        }
        super.send(opt_body);
    }
};