function saveOpportunityLog(node) {
    chrome.storage.local.get('opportunity_log', function(result) {
        let opportunity_log = result.opportunity_log || [];
        opportunity_log = opportunity_log.concat(JSON.parse(node.innerText));
        chrome.storage.local.set({opportunity_log}, function() {
          node.innerText = '[]';
        });
    });
}

function watchOpportunityLog() {
    const node = document.getElementById('opportunity-log');
    if (!node) {
        console.error('ERROR: Draw logging cannot be enabled.');
        return;
    }
    saveOpportunityLog(node);
    
    node.addEventListener('updatejson', (e) => {
        saveOpportunityLog(e.currentTarget);
    });
}

watchOpportunityLog();