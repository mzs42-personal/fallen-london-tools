function leadingZeroes(str, len) {
    str = '' + str;
    while (str.length < len) { str = '0' + str; }
    return str;
}

function formatDate(date) {
    const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    return `${months[date.getUTCMonth()]} ${leadingZeroes(date.getUTCDate(), 2)} ${date.getUTCFullYear()} ${leadingZeroes(date.getUTCHours(), 2)}:${leadingZeroes(date.getUTCMinutes(), 2)}:${leadingZeroes(date.getUTCSeconds(), 2)}.${leadingZeroes(date.getUTCMilliseconds(), 3)}`;
}

function clearContainer(log_container) {
    for (let childNode of log_container.childNodes) {
        log_container.removeChild(childNode);
    }
}

function showRawData() {
    const log_container = document.getElementById('opportunity-log');
    const log_text = document.getElementById('raw_data').innerText;
    
    clearContainer(log_container);

    const pre_el = document.createElement('pre');
    const code_el = document.createElement('code');
    code_el.innerText = log_text;
    pre_el.appendChild(code_el);
    log_container.appendChild(pre_el);
}

function showFormattedData() {
    const log_container = document.getElementById('opportunity-log');
    const log_text = document.getElementById('raw_data').innerText;
    
    clearContainer(log_container);

    const table_el = document.createElement('table');
    const header_row = document.createElement('tr');
    const headers = [
        header_row.appendChild(document.createElement('th')),
        header_row.appendChild(document.createElement('th')),
        header_row.appendChild(document.createElement('th')),
        header_row.appendChild(document.createElement('th')),
    ];
    headers[0].innerText = 'Timestamp';
    headers[1].innerText = 'Operation';
    headers[2].innerText = 'Card ID';
    headers[3].innerText = 'Card Name';
    table_el.appendChild(header_row);
    for (let json_row of JSON.parse(log_text)) {
        const row = document.createElement('tr');
        const cells = [
            row.appendChild(document.createElement('td')),
            row.appendChild(document.createElement('td')),
            row.appendChild(document.createElement('td')),
            row.appendChild(document.createElement('td')),
        ];
        cells[0].innerText = formatDate(new Date(json_row.timestamp));
        cells[1].innerText = json_row.operation;
        cells[1].classList.add('operation');
        cells[2].innerText = json_row.card_id;
        cells[3].innerText = json_row.card_name;
        table_el.appendChild(row);
    }
    log_container.appendChild(table_el);
}

function toggleButton(toggle) {
    if (toggle.classList.contains('view-raw')) {
        toggle.classList.add('view-readable');
        toggle.classList.remove('view-raw');
        toggle.innerText = 'View formatted data';
    } else {
        toggle.classList.add('view-raw');
        toggle.classList.remove('view-readable');
        toggle.innerText = 'View raw JSON data';
    }
}

function showData(should_toggle) {
    const toggle = document.getElementById('toggle-view');
    if (should_toggle) { toggleButton(toggle); }

    if (toggle.classList.contains('view-readable')) {
        showRawData();
    } else {
        showFormattedData();
    }
}

function loadData() {
    chrome.storage.local.get('opportunity_log', function(result) {
        document.getElementById('raw_data').innerText = JSON.stringify(result.opportunity_log || []);
        showData(true);
    });
}

function clearData() {
    chrome.storage.local.set({opportunity_log: []});
    showData();
}

function loadPage() {
    loadData();
    document.getElementById('toggle-view').addEventListener('click', showData);
    document.getElementById('clear-data').addEventListener('click', clearData);
}

loadPage();