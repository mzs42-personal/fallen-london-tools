(() => {
	const PERMALINK = document.getElementById('permalink');
	const NICE_K_ASTERISK = document.getElementById('nice-k-asterisk');
	const K_STEP_INPUT = document.getElementById('k-step');
	const NICE_K_CHECKBOX = document.getElementById('k-nice');
	const CANDIDATES_INPUT = document.getElementById('candidates');
	const CANDIDATE_BUTTONS = document.getElementById('candidate-buttons');
	const COPY_BUTTON = document.getElementById('copy-button');
	const TABLE_SECTION = document.getElementById('table-section');
	const GRAPH_SECTION = document.getElementById('graph-section');
	const GRIDLINE_COLOUR = '#444444';
	let CURRENT_CHART = null;

	const gcd = (a, b) => {
		// Stein's algorithm
		a = Math.abs(a);
		b = Math.abs(b);
		if (a == 0) {
			return b;
		}
		if (b == 0) {
			return a;
		}

		let s = 0
		while (!((a | b) & 1)) {
			a >>= 1;
			b >>= 1;
			++s;
		}

		while (!(a & 1)) {
			a >>= 1;
		}

		while (!(b & 1)) {
			b >>= 1;
		}

		while (a != b) {
			[a, b] = [Math.abs(a - b), Math.min(a, b)]
			while (!(a & 1)) {
				a >>= 1;
			}
		}

		return a << s;
	};

	const round = (x) => {
		if (x == 0) {
			return 0;
		}
		const ax = Math.abs(x);
		const frac = x % 1;
		if ((frac == .5 && Math.ceil(ax) % 2 == 0) || frac > .5) {
			return Math.sign(x) * Math.ceil(ax);
		}
		if (Math.floor(ax) == 0) {
			return 0;
		}
		return Math.sign(x) * Math.floor(ax);
	};

	class SCurveCandidate {
		constructor(height, k, midpoint, y_offset) {
			this.height = height;
			this.k = k;
			this.midpoint = midpoint;
			this.y_offset = y_offset;
		}
		calculate(x, do_round = true) {
			const val = this.y_offset + this.height / (1 + Math.exp(-this.k * (x - this.midpoint)));
			if (do_round) {
				return round(val);
			}
			return val;
		}
		test_curve(data) {
			for (let dp of data) {
				if (dp[1] != this.calculate(dp[0])) {
					return false;
				}
			}
			return true;
		}
		bound1() {
			return this.midpoint - Math.log(2 * Math.abs(this.height) - 1) / this.k;
		}
		bound2() {
			const height_frac = Math.abs(this.height) / (Math.abs(this.height) - .5)
			return this.midpoint - Math.log(height_frac - 1) / this.k;
		}
		left_bound() {
			if (this.k < 0) {
				return Math.floor(this.bound2());
			}
			return Math.floor(this.bound1());
		}
		right_bound() {
			if (this.k < 0) {
				return Math.ceil(this.bound1());
			}
			return Math.ceil(this.bound2());
		}
		toString() {
			return `height = ${formatted(this.height)}, k = ${formatted(this.k)}, midpoint = ${formatted(this.midpoint)}, y offset = ${formatted(this.y_offset)}`;
		}
	}

	class Fraction {
		constructor(numerator, denominator) {
			this.numerator = numerator;
			this.denominator = denominator;
		}
		reduce() {
			const div = gcd(this.numerator, this.denominator);
			this.numerator /= div;
			this.denominator /= div;
		}
		reduced() {
			const div = gcd(this.numerator, this.denominator);
			return new Fraction(
				this.numerator / div,
				this.denominator / div);
		}
		equals(other) {
			if (other instanceof Fraction) {
				// avoid float comparisons if possible
				const thisreduced = this.reduced();
				const otherreduced = other.reduced();
				return thisreduced.numerator == otherreduced.numerator && thisreduced.denominator == otherreduced.denominator;
			}
			return this.numerator / this.denominator == other;
		}
		valueOf() {
			return this.numerator / this.denominator;
		}
		toString() {
			if (this.denominator == 1) {
				return `${this.numerator}`;
			}
			return `${this.numerator}/${this.denominator}`;
		}
	}

	class Range {
		constructor(name, start, end, step, nice=false) {
			this.name = name;
			this.start = start;
			this.end = end;
			if (start > end) {
				this.start = end;
				this.end = start;
			}
			this.step = step;
			this.nice = nice;
			if (!nice) {
				if (step < 0) {
					this.step = -step;
				} else if (step == 0) {
					if (start == end) {
						this.step = 1;
					} else {
						CANDIDATES_INPUT.replaceChildren
							(make_option(`${this.name} step must be non-zero`));
						throw `${this.name} step must be non-zero`;
					}
				}
			}
			this.fractions = null;
		}

		get_fractions() {
			const used = new Set();
			const fractions = [];
			for (let denom = 10; denom > 5; --denom) {
				for (let numer = Math.ceil(this.start * denom); numer <= Math.floor(this.end * denom); ++numer) {
					if (used.has(numer / denom)) {
						continue;
					}
					const frac = new Fraction(numer, denom);
					frac.reduce();
					fractions.push(frac);
					used.add(frac.valueOf());
				}
			}
			return fractions;
		}

		[Symbol.iterator]() {
			if (this.nice) {
				if (!this.fractions) {
					this.fractions = this.get_fractions();
				}
				let i = 0;

				return {
					next: () => {
						if (i < this.fractions.length) {
							return {value: this.fractions[i++], done: false};
						} else {
							return {done: true};
						}
					}
				};
			} else {
				let val = this.start;

				return {
					next: () => {
						if (val <= this.end) {
							const ret_val = val;
							val += this.step;
							return {value: ret_val, done: false};
						} else {
							return {done: true};
						}
					}
				};
			}
		}
	}

	const formatted = (num, places = 3) => {
		if (num instanceof Fraction) {
			return num;
		}
		return num.toFixed(places).replace(/(\.|(\.\d+?))0+$/, '$2');
	};

	const get_candidates = (data, height_range, k_range, midpoint_range, y_offset_range) => {
		const candidates = [];
		for (let height of height_range) {
			for (let k of k_range) {
				for (let midpoint of midpoint_range) {
					for (let y_offset of y_offset_range) {
						const candidate = new SCurveCandidate(height, k, midpoint, y_offset);
						if (candidate.test_curve(data)) {
							candidates.push(candidate);
						}
					}
				}
			}
		}
		return candidates;
	};

	const get_float = (id, type) => {
		return Number.parseFloat(document.getElementById(`${id}-${type}`).value);
	};

	const make_range = (name, id, has_nice) => {
		const nice = has_nice && document.getElementById(`${id}-nice`).checked;
		return new Range(
			name, get_float(id, 'start'), get_float(id, 'end'),
			get_float(id, 'step'), nice);
	};

	const make_element = (tag_name, text) => {
		const el = document.createElement(tag_name);
		el.textContent = text;
		return el;
	};

	const find_candidates = () => {
		if (CURRENT_CHART) {
			CURRENT_CHART.destroy();
			CURRENT_CHART = null;
		}
		TABLE_SECTION.hidden = true;
		GRAPH_SECTION.hidden = true;
		for (let button of CANDIDATE_BUTTONS.childNodes) {
			button.disabled = true;
		}
		const height_range = make_range('Height', 'height', false);
		const k_range = make_range('k', 'k', true)
		const midpoint_range = make_range('Midpoint', 'midpoint', false);
		const y_offset_range = make_range('Y Offset', 'y-offset', false);
		const data_tokens = document.getElementById('data').value.split(/\s+/);
		if (data_tokens.length % 2) {
			console.error('Unable to read data');
			return;
		}
		const data = [];
		for (let i = 0; i < data_tokens.length; i += 2) {
			data.push([Number.parseFloat(data_tokens[i]), Number.parseFloat(data_tokens[i+ 1])]);
		}
		const candidates = get_candidates(data, height_range, k_range, midpoint_range, y_offset_range);
		const options = [];
		for (let candidate of candidates) {
			const opt = make_element('option', candidate);
			opt.candidate = candidate;
			options.push(opt);
		}
		CANDIDATES_INPUT.replaceChildren(...options);
		if (options.length) {
			for (let button of CANDIDATE_BUTTONS.childNodes) {
				button.disabled = false;
			}
		}
	};

	const get_params_from_url = () => {
		const params = new URLSearchParams(window.location.search);
		for (let param of params.entries()) {
			if (param[0] == 'k-nice' && param[1] == 'true') {
				NICE_K_CHECKBOX.checked = true;
				K_STEP_INPUT.disabled = true;
			} else {
				document.getElementById(param[0]).value = param[1];
			}
		}
	};

	const make_permalink = (e) => {
		const input = e.target;
		const tag_name = input.tagName.toLowerCase();
		if (tag_name != 'textarea' && tag_name != 'input') {
			return;
		}
		const url = new URL(PERMALINK.href);
		const params = url.searchParams;
		if (input.type == 'checkbox') {
			if (input.checked) {
				params.set(input.id, 'true');
			} else {
				params.delete(input.id);
			}
		} else {
			let param_value = input.value;
			if (param_value == null) {
				param_value = '';
			}
			params.set(input.id, param_value);
		}
		PERMALINK.href = url.toString();
	};

	const load_page = () => {
		PERMALINK.href = window.location;
		get_params_from_url();
		document.getElementById('find-candidates').addEventListener('click', find_candidates);
		for (let input of document.querySelectorAll('#data,input[type="number"]')) {
			input.addEventListener('input', make_permalink);
		}
		NICE_K_ASTERISK.addEventListener('click', (e) => {
			e.preventDefault();
		});
		NICE_K_CHECKBOX.addEventListener('change', (e) => {
			if (NICE_K_CHECKBOX.checked) {
				K_STEP_INPUT.disabled = true;
			} else {
				K_STEP_INPUT.disabled = false;
			}
			make_permalink(e);
		});
		COPY_BUTTON.addEventListener('click', () => {
			const opt = CANDIDATES_INPUT.selectedOptions[0];
			if (!opt) {
				return;
			}
			navigator.clipboard.writeText(opt.textContent).then(() => {
				COPY_BUTTON.textContent = 'Copied!'
				window.setTimeout(() => {COPY_BUTTON.textContent = 'Copy'}, 1000);
			}, (err) => {
				console.error('Copy failed', err);
				COPY_BUTTON.textContent = 'Failed!'
				window.setTimeout(() => {COPY_BUTTON.textContent = 'Copy'}, 1000);
			});
		});
		document.getElementById('table-button').addEventListener('click', () => {
			const opt = CANDIDATES_INPUT.selectedOptions[0];
			if (!opt || !opt.candidate) {
				return;
			}
			TABLE_SECTION.hidden = false;
			GRAPH_SECTION.hidden = true;
			const candidate = opt.candidate;
			const caption = make_element('caption', candidate);
			const th_row = document.createElement('tr');
			th_row.append(
				make_element('th', 'x'),
				make_element('th', 'y'),
				make_element('th', 'y (rounded)')
			);
			const td_rows = [];
			const right_bound = candidate.right_bound();
			for (let i = candidate.left_bound() - 1; i < right_bound + 2; ++i) {
				const y_val = candidate.calculate(i, false);
				const row = document.createElement('tr');
				row.append(
					make_element('td', i),
					make_element('td', formatted(y_val)),
					make_element('td', round(y_val))
				);
				td_rows.push(row);
			}
			document.getElementById('table').replaceChildren(
				caption, th_row, ...td_rows);
		});
		document.getElementById('graph-button').addEventListener('click', () => {
			const opt = CANDIDATES_INPUT.selectedOptions[0];
			if (!opt || !opt.candidate) {
				return;
			}
			if (CURRENT_CHART) {
				CURRENT_CHART.destroy();
			}
			TABLE_SECTION.hidden = true;
			GRAPH_SECTION.hidden = false;
			
			const candidate = opt.candidate;
			const x = [];
			const y = [];
			const right_bound = candidate.right_bound();
			for (let i = candidate.left_bound() - 2; i < right_bound + 3; ++i) {
				x.push(i);
				y.push(candidate.calculate(i, false));
			}

			let ctx = document.getElementById('graph').getContext('2d');
			CURRENT_CHART = new Chart(ctx, {
				type: 'line',
				data: {
					labels: x,
					datasets: [
						{
							label: candidate,
							data: y,
							borderColor: '#42686b',
							backgroundColor: '#2d5256',
							tension: 0.1,
							fill: false,
						},
					],
				},
				options: {
					plugins: {
						title: {
							color: GRIDLINE_COLOUR,
							text: candidate.toString(),
							display: true,
						},
						tooltip: {
							displayColors: false,
							callbacks: {
								label: function(context) {
									return round(context.parsed.y);
								}
							}
						},
						legend: {
							display: false,
						},
					},
					scales: {
						x: {
							ticks: {
								color: GRIDLINE_COLOUR,
							},
							grid: {
								color: GRIDLINE_COLOUR,
							},
						},
						y: {
							ticks: {
								color: GRIDLINE_COLOUR,
							},
							grid: {
								color: GRIDLINE_COLOUR,
							},
						},
					},
				},
			});
		});
	};

	load_page();
})();