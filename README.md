This is just a collection of whatever random Fallen London tools I've written and deemed okay enough to release and show other people. See each directory for more details of each.

Included tools:

* Fallen London Draw Logger: A quickly-built Chrome extension to log which cards you draw and discard and when. Intended to help with data collection for [this Reddit post](https://www.reddit.com/r/fallenlondon/comments/k6wlrl/deck_redraw_bug_testing_help_wanted/)
